package dwfe.modules.storage.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.RestTemplateCustomizer;
import org.springframework.cloud.netflix.ribbon.RibbonClientHttpRequestFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StorageRestClientConfig
{
  /**
   * Customize the RestTemplate to use Ribbon load balancer to resolve service endpoints
   */
  @Bean
  public RestTemplateCustomizer ribbonClientRestTemplateCustomizer(
          final RibbonClientHttpRequestFactory ribbonClientHttpRequestFactory)
  {
    return (@Qualifier("RibbonRestTemplate") RestTemplate restTemplate)
            -> restTemplate.setRequestFactory(ribbonClientHttpRequestFactory);
  }
}


