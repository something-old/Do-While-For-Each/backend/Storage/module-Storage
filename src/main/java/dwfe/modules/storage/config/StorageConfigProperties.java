package dwfe.modules.storage.config;

import dwfe.util.DwfeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe.storage")
public class StorageConfigProperties implements InitializingBean
{
  // == http://www.baeldung.com/configuration-properties-in-spring-boot

  private final static Logger log = LoggerFactory.getLogger(StorageConfigProperties.class);

  private String api;

  @NotNull
  @NotEmpty
  private String rootPath;

  @NotNull
  @NotEmpty
  private String rootDirName;

  private Path rootDirPath;
  private String rootUrl;

  @NotNull
  @Size(min = 1)
  private Set<String> directories;

  @NotNull
  @NotEmpty
  private String frontendHost;

  private Resource resource = new Resource();

  @Override
  public void afterPropertiesSet() throws Exception
  {
    rootDirPath = Paths.get(rootPath, rootDirName);
    rootUrl = DwfeUtil.listToUnixPath(List.of(
            frontendHost,
            rootDirName), false);

    if (Files.exists(rootDirPath)) // is rootDir exists?
    {
      for (var dirName : directories)
      {
        var dirPath = rootDirPath.resolve(dirName);
        if (!Files.exists(dirPath)) // is valid directory exists?
        {
          Files.createDirectory(dirPath); // create valid directory if it not exist
        }
        var testDirPath = Files.createDirectory(dirPath.resolve("testDir"));
        var testFilePath = Files.createFile(dirPath.resolve("test.txt"));
        Files.delete(testDirPath);
        Files.delete(testFilePath);
      }
    }
    else
    {
      throw new Exception(String.format("%n%n --> root-dir-path is not exists%n%n"));
    }

    log.info(toString());
  }

  public static class Resource
  {
    private String uploadByUser = "/upload-by-user/{directory}";

    public String getUploadByUser()
    {
      return uploadByUser;
    }

    public void setUploadByUser(String uploadByUser)
    {
      this.uploadByUser = uploadByUser;
    }
  }

  public String getApi()
  {
    return api;
  }

  public void setApi(String api)
  {
    this.api = api;
  }

  public String getRootPath()
  {
    return rootPath;
  }

  public void setRootPath(String rootPath)
  {
    this.rootPath = rootPath;
  }

  public String getRootUrl()
  {
    return rootUrl;
  }

  public void setRootUrl(String rootUrl)
  {
    this.rootUrl = rootUrl;
  }

  public String getRootDirName()
  {
    return rootDirName;
  }

  public void setRootDirName(String rootDirName)
  {
    this.rootDirName = rootDirName;
  }

  public Path getRootDirPath()
  {
    return rootDirPath;
  }

  public void setRootDirPath(Path rootDirPath)
  {
    this.rootDirPath = rootDirPath;
  }

  public Set<String> getDirectories()
  {
    return directories;
  }

  public void setDirectories(Set<String> directories)
  {
    this.directories = directories;
  }

  public String getFrontendHost()
  {
    return frontendHost;
  }

  public void setFrontendHost(String frontendHost)
  {
    this.frontendHost = frontendHost;
  }

  public Resource getResource()
  {
    return resource;
  }

  public void setResource(Resource resource)
  {
    this.resource = resource;
  }

  @Override
  public String toString()
  {
    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                       Storage                      |%n" +
                    "|----------------------------------------------------|%n" +
                    "|                                                     %n" +
                    "| API version            %s%n" +
                    "|                                                     %n" +
                    "| Storage Root Dir       %s%n" +
                    "| Storage Root URL       %s%n" +
                    "|                                                     %n" +
                    "| Valid directories      %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| API Resources                                       %n" +
                    "|                                                     %n" +
                    "|   %s%n" +
                    "|_____________________________________________________%n%n",
            api,

            rootDirPath,
            rootUrl,

            directories,

            resource.uploadByUser
    );
  }
}
