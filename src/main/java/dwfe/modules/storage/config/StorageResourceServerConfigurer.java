package dwfe.modules.storage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableOAuth2Client
@EnableResourceServer
public class StorageResourceServerConfigurer extends ResourceServerConfigurerAdapter
{
  @Autowired
  private RemoteTokenServices remoteTokenServices;

  @Override
  public void configure(HttpSecurity http) throws Exception
  {
    http
            //you don't need protection from CSRF, because RESTful
            .csrf().disable()

            //you don't need sessions, because RESTful
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            //but the session is still created: HttpServletRequest request, request.getSession()

            //for prevent exception: Cannot apply ExpressionUrlAuthorizationConfigurer
            .authorizeRequests().anyRequest().permitAll()
    ;
  }

  @Bean("RibbonRestTemplate")
  @LoadBalanced
  public RestTemplate restTemplate()
  {
    var restTemplate = new RestTemplate();

    // == https://github.com/spring-cloud/spring-cloud-security/issues/61#issuecomment-420348100
    remoteTokenServices.setRestTemplate(restTemplate);

    return restTemplate;
  }
}
