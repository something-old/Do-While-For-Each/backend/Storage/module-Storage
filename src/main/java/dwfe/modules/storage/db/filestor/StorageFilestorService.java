package dwfe.modules.storage.db.filestor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Primary
@Transactional(readOnly = true)
public class StorageFilestorService
{
  private final StorageFilestorRepository filestorRepository;

  @Autowired
  public StorageFilestorService(StorageFilestorRepository filestorRepository)
  {
    this.filestorRepository = filestorRepository;
  }

  @Transactional
  public StorageFilestor save(StorageFilestor filestor)
  {
    return filestorRepository.save(filestor);
  }
}
