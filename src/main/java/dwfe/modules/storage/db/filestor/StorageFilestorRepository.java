package dwfe.modules.storage.db.filestor;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StorageFilestorRepository extends JpaRepository<StorageFilestor, String>
{
}
