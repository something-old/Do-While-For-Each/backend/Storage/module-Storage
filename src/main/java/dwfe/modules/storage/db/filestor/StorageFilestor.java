package dwfe.modules.storage.db.filestor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "dwfe_storage_filestor")
public class StorageFilestor
{
  @Id
  private String id;
  private String relativePath;
  private String directory;
  private String userId;
  private String fileName;
  private Long fileSize;

  @Column(updatable = false, insertable = false)
  private LocalDateTime createdOn;

  @Column(updatable = false, insertable = false)
  private LocalDateTime updatedOn;


  //
  //  GETTERs and SETTERs
  //

  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public String getRelativePath()
  {
    return relativePath;
  }

  public void setRelativePath(String relativePath)
  {
    this.relativePath = relativePath;
  }

  public String getUserId()
  {
    return userId;
  }

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getDirectory()
  {
    return directory;
  }

  public void setDirectory(String directory)
  {
    this.directory = directory;
  }

  public String getFileName()
  {
    return fileName;
  }

  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }

  public Long getFileSize()
  {
    return fileSize;
  }

  public void setFileSize(Long fileSize)
  {
    this.fileSize = fileSize;
  }

  public LocalDateTime getCreatedOn()
  {
    return createdOn;
  }

  public void setCreatedOn(LocalDateTime createdOn)
  {
    this.createdOn = createdOn;
  }

  public LocalDateTime getUpdatedOn()
  {
    return updatedOn;
  }

  public void setUpdatedOn(LocalDateTime updatedOn)
  {
    this.updatedOn = updatedOn;
  }


  //
  //  equals, hashCode
  //

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    StorageFilestor that = (StorageFilestor) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode()
  {
    return id.hashCode();
  }
}
