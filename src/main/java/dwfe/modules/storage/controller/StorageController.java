package dwfe.modules.storage.controller;

import dwfe.modules.storage.config.StorageConfigProperties;
import dwfe.modules.storage.db.filestor.StorageFilestor;
import dwfe.modules.storage.db.filestor.StorageFilestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static dwfe.util.DwfeUtil.getResponse;
import static dwfe.util.DwfeUtil.listToUnixPath;

@RestController
@RequestMapping(path = "#{storageConfigProperties.api}", produces = "application/json; charset=utf-8")
public class StorageController
{
  private final StorageConfigProperties propStorage;
  private final StorageFilestorService filestorService;

  @Autowired
  public StorageController(StorageConfigProperties propStorage, StorageFilestorService filestorService)
  {
    this.propStorage = propStorage;
    this.filestorService = filestorService;
  }

  //
  // == https://github.com/spring-guides/gs-uploading-files/blob/master/complete/src/main/java/hello/FileUploadController.java
  //
  @PreAuthorize("hasAuthority('STORAGE_USER')")
  @PostMapping("#{storageConfigProperties.resource.uploadByUser}")
  public String uploadByUser(@PathVariable("directory") String directoryName,
                             @RequestParam("file") MultipartFile file,
                             Principal principal) throws IOException
  {
    var errorCodes = new ArrayList<String>();
    var rootDirPath = propStorage.getRootDirPath().toString();
    var userDirName = principal.getName();
    var fileName = StringUtils.cleanPath(file.getOriginalFilename());
    var respMap = new HashMap<String, Object>();

    if (!propStorage.getDirectories().contains(directoryName))
      errorCodes.add("invalid-directory");
    else if (fileName.isEmpty() || fileName.contains(".."))
      errorCodes.add("invalid-filename");

    if (errorCodes.size() == 0)
    {
      var targetDirPath = Paths.get(rootDirPath, directoryName, userDirName);
      if (!(Files.exists(targetDirPath) && Files.isDirectory(targetDirPath)))
        Files.createDirectory(targetDirPath);

      try (var inputStream = file.getInputStream())
      {
        Files.copy(inputStream, targetDirPath.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
      }

      var relativePath = listToUnixPath(List.of(
              directoryName,
              userDirName,
              fileName), false);

      var filestorDb = new StorageFilestor();
      filestorDb.setId(UUID.randomUUID().toString());
      filestorDb.setRelativePath(relativePath);
      filestorDb.setDirectory(directoryName);
      filestorDb.setUserId(userDirName);
      filestorDb.setFileName(fileName);
      filestorDb.setFileSize(file.getSize());
      filestorService.save(filestorDb);

      respMap.put("url", listToUnixPath(List.of(
              propStorage.getRootUrl(),
              relativePath), false));
      respMap.put("relativePath", relativePath);
      respMap.put("id", filestorDb.getId());
    }
    return getResponse(errorCodes, respMap);
  }
}
