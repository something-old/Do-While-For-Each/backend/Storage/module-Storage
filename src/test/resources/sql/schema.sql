USE dwfe_test;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS
dwfe_storage_filestor;
SET FOREIGN_KEY_CHECKS = 1;

--
-- MODULE: Storage
--

CREATE TABLE dwfe_storage_filestor (
  created_on    DATETIME                             NOT NULL                DEFAULT CURRENT_TIMESTAMP,
  relative_path VARCHAR(2000)                        NOT NULL,
  directory     VARCHAR(100),
  user_id       VARCHAR(20)                          NOT NULL,
  file_name     VARCHAR(100)                         NOT NULL,
  file_size     BIGINT(20)                           NOT NULL,
  id            VARCHAR(36)                          NOT NULL,
  updated_on    DATETIME ON UPDATE CURRENT_TIMESTAMP NOT NULL                DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;