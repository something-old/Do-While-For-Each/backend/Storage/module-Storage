package dwfe.modules.storage;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.storage.config.StorageConfigProperties;
import dwfe.test.config.DwfeTestLevelAuthority;
import dwfe.test.config.DwfeTestVariablesForAuthAccessTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Component
public class StorageTestVariablesForAuthAccessTest extends DwfeTestVariablesForAuthAccessTest
{
  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private StorageConfigProperties propStorage;

  @Override
  public Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> RESOURCE_AUTHORITY_reqDATA()
  {
    return Map.of();
  }
}
