package dwfe.modules.storage;

import dwfe.test.DwfeTestChecker;

import java.util.List;
import java.util.Map;

public class StorageTestVariablesForApiTest
{

  //-------------------------------------------------------
  // Personal
  //

  public static final List<DwfeTestChecker> checkers_for_uploadByUser = List.of(
          DwfeTestChecker.of(false, 200, Map.of(), "missing-module"),
          DwfeTestChecker.of(false, 200, Map.of("module", ""), "empty-module")

//          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "EMAIL_CONFIRM", "email", "test1@dwfe.ru", "data", "kjsjkjsksjsksj"))
  );
}
