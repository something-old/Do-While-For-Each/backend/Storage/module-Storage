package dwfe.modules.storage;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.storage.config.StorageConfigProperties;
import dwfe.test.DwfeTestAuth;
import dwfe.test.DwfeTestUtil;
import dwfe.test.config.DwfeTestConfigProperties;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Map;

import static dwfe.test.DwfeTestUtil.getTestFile;
import static dwfe.test.DwfeTestUtil.pleaseWait;
import static org.junit.Assert.*;

//
// == https://spring.io/guides/gs/testing-web/
//

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT  // == https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StorageTest
{
  private static final Logger log = LoggerFactory.getLogger(StorageTest.class);

  @Autowired
  private StorageConfigProperties propStorage;
  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private DwfeTestUtil utilTest;
  @Autowired
  private DwfeTestAuth auth;

  @Autowired
  RestTemplateBuilder restTemplateBuilder;


  //-------------------------------------------------------
  // ACTUATOR
  //

  @Test
  public void _00_01_actuator()
  {
    pleaseWait(65, log);
    utilTest.test_actuator();
  }


  //-------------------------------------------------------
  // File Storage
  //


  @Test
  public void _01_01_uploadByUser() throws IOException
  {
    var resource = "/upload-by-user/";
    var directory = propStorage.getDirectories().iterator().next();
    var url = storageApiRoot() + resource + directory;
    var restTemplate = restTemplateBuilder.build();
    var typeReference = new ParameterizedTypeReference<Map<String, Object>>()
    {
    };
    var userAccessToken = auth.getUSER().access_token;
    var fileToSend = new LinkedMultiValueMap<String, Object>();
    fileToSend.add("file", getTestFile());
    var successField = "success";
    var errorCodesField = "error-codes";


    // == invalid-directory

    var req = RequestEntity
            .post(URI.create(url + "1"))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .header("Authorization", "Bearer " + userAccessToken)
            .body(fileToSend);

    var resp = restTemplate.exchange(req, typeReference);
    var respBody = resp.getBody();
    assertEquals(200, resp.getStatusCodeValue());
    assertFalse((Boolean) respBody.get(successField));
    assertTrue(respBody.containsKey(errorCodesField));
    var errorCodes = (ArrayList<String>) respBody.get(errorCodesField);
    assertEquals("invalid-directory", errorCodes.get(0));


    // == 500 Internal Server Error

    req = RequestEntity
            .post(URI.create(url))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .header("Authorization", "Bearer " + userAccessToken + "1")
            .body(fileToSend);
    try
    {
      resp = restTemplate.exchange(req, typeReference);
    }
    catch (RestClientException ex)
    {
      assertEquals("500 Internal Server Error", ex.getMessage());
    }


    // == 401 Unauthorized

    req = RequestEntity
            .post(URI.create(url))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .body(fileToSend);
    try
    {
      resp = restTemplate.exchange(req, typeReference);
    }
    catch (RestClientException ex)
    {
      assertEquals("401 Unauthorized", ex.getMessage());
    }


    // == Success

    req = RequestEntity
            .post(URI.create(url))
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .header("Authorization", "Bearer " + userAccessToken)
            .body(fileToSend);

    resp = restTemplate.exchange(req, typeReference);
    respBody = resp.getBody();
    assertEquals(200, resp.getStatusCodeValue());
    assertTrue((Boolean) respBody.get(successField));
    assertFalse(respBody.containsKey(errorCodesField));
  }


  //-------------------------------------------------------
  // UTILs
  //

  private String storageApiRoot()
  {
    return propDwfe.getService().getGatewayUrl() + propDwfe.getService().getStorage().getGatewayPath();
  }
}
