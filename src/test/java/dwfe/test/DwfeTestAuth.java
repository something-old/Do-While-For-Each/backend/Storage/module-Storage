package dwfe.test;

import dwfe.test.config.DwfeTestAccountUsernameType;
import dwfe.test.config.DwfeTestConfigProperties;
import dwfe.test.config.DwfeTestLevelAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static dwfe.test.config.DwfeTestAuthType.SIGN_IN;
import static dwfe.test.config.DwfeTestLevelAuthority.ADMIN;
import static dwfe.test.config.DwfeTestLevelAuthority.USER;

@Component
public class DwfeTestAuth
{
  @Autowired
  private DwfeTestConfigProperties propTest;
  @Autowired
  private DwfeTestUtil utilTest;
  @Autowired
  private DwfeTestClient clientTest;

  public String username;
  public DwfeTestAccountUsernameType usernameType;
  public String password;

  public DwfeTestClient client;

  public DwfeTestLevelAuthority levelAuthority;

  public String access_token;
  public String refresh_token;


  public DwfeTestAuth of(DwfeTestLevelAuthority authorityLevel,
                         String username, DwfeTestAccountUsernameType usernameType,
                         String password,
                         DwfeTestClient client)
  {
    var auth = new DwfeTestAuth();
    auth.levelAuthority = authorityLevel;
    auth.username = username;
    auth.usernameType = usernameType;
    auth.password = password;
    auth.client = client;

    setToken(auth);
    return auth;
  }

  public DwfeTestAuth of(String username, DwfeTestAccountUsernameType usernameType,
                         String password,
                         DwfeTestClient client)
  {
    var auth = new DwfeTestAuth();
    auth.levelAuthority = USER;
    auth.username = username;
    auth.usernameType = usernameType;
    auth.password = password;
    auth.client = client;

    return auth;
  }


  private void setToken(DwfeTestAuth auth)
  {
    utilTest.tokenProcess(SIGN_IN, auth, 200);
  }

  public DwfeTestAuth getAnonymous()
  {
    var auth = new DwfeTestAuth();
    auth.levelAuthority = DwfeTestLevelAuthority.ANY;
    return auth;
  }

  public DwfeTestAuth getADMIN()
  {
    return of(ADMIN, propTest.getLevelAdmin().getUsername(), null, propTest.getLevelAdmin().getPassword(), clientTest.getClientUntrusted());
  }

  public DwfeTestAuth getUSER()
  {
    return of(USER, propTest.getLevelUser().getUsername(), null, propTest.getLevelUser().getPassword(), clientTest.getClientTrusted());
  }

  public String getAnonym_accessToken()
  {
    return getAnonymous().access_token;
  }
}

